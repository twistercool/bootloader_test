all: boot_disk.img

# creates the boot disk which you can use with an emulator (or real hardware if you decide to dd it to an MBR drive, though I wouldn't recommend it...)
boot_disk.img: boot.o
	@if [ ! -f 'boot_disk.img' ]; then echo "Creating boot disk..."; \
		qemu-img create -f raw boot_disk.img 8M; \
		printf "\x55\xAA" | dd bs=1 seek=510 status=none conv=notrunc of=boot_disk.img; # Marks boot sector as actually bootable by writing two specific bytes at a specific position in the sector \
		fi
	dd status=none if=boot.o conv=notrunc of=boot_disk.img
	@echo "Created boot disk"

# Assembles the 
boot.o: src/boot_sector.asm
	nasm src/boot_sector.asm -f bin -o boot.o
	@boot_size=$$(stat -c '%s' boot.o); \
		if [ "$${boot_size}" -ge 446 ]; then echo "Boot exec too big!"; exit 1 ; fi

.PHONY: all
