%define KEY_LEFT 'a'
%define KEY_UP 'w'
%define KEY_RIGHT 'd'
%define KEY_DOWN 's'
%define MAX_HEIGHT 22 ; counting 0 and borders
%define MAX_WIDTH 79  ; counting 0 and borders
%xdefine MAX_HEIGHT_BORDER MAX_HEIGHT - 1
%xdefine MAX_WIDTH_BORDER MAX_WIDTH - 1

[bits 16]
[org 0x7c00]

   mov ax, 0
   mov ds, ax
   mov ss, ax
   mov es, ax
   mov fs, ax
   mov gs, ax
   mov sp, ax
   mov si, ax

; fs, gs and es are extra segment registers
; cx-> counter register
; di -> destination reg
; si -> source reg
; dx -> data reg

; Init game logic

; make cursor invisible
   mov ah, 0x1
   mov ch, 0b0_01_00000
   mov cl, 0b0
   int 0x10

   call clear_screen

   call draw_border

; Apparently can't push single bytes?
%define food_x_w word [si + 0xa]
%define food_x byte [si + 0xa]
   push word 6
%define food_y_w word [si + 0x8]
%define food_y byte [si + 0x8]
   push word 8
%define dir_x_w word [si + 0x6]
%define dir_x byte [si + 0x6]
   push word 1
%define dir_y_w word [si + 0x4]
%define dir_y byte [si + 0x4]
   push word 0
%define cur_x_w word [si + 0x2]
%define cur_x byte [si + 0x2]
   push word 5
%define cur_y_w word [si]
%define cur_y byte [si]
   push word 5
; don't clobber 'si'!
; Can't dereference the stack pointer apparently ;-;
   mov si, sp


   mov al, '$'
   call draw_at_snake_loc
   mov al, '*'
   call draw_at_food_loc

.main_loop:
; take input ("non-blocking" wait)
   mov ah, 0
   ; mov ah, 1
   int 0x16
   jz .end_input

   cmp al, KEY_LEFT
   jne .next_up
   mov dir_x, -1
   mov dir_y, 0
   jmp .end_input
.next_up:
   cmp al, KEY_UP
   jne .next_right
   mov dir_x, 0
   mov dir_y, -1
   jmp .end_input
.next_right:
   cmp al, KEY_RIGHT
   jne .next_down
   mov dir_x, 1
   mov dir_y, 0
   jmp .end_input
.next_down:
   cmp al, KEY_DOWN
   jne .end_input
   mov dir_x, 0
   mov dir_y, 1
.end_input:

; unpaint snake at cur position
   mov al, ' '
   call draw_at_snake_loc

; move snake towards dist
   mov dl, dir_x
   mov dh, dir_y
   add cur_x, dl
   add cur_y, dh
; if less than 0, reset to 0
   cmp cur_x, 1
   jge .dont_zero_curx
   mov cur_x, 1
.dont_zero_curx:
   cmp cur_y, 1
   jge .dont_zero_cury
   mov cur_y, 1
.dont_zero_cury:
; if more than max, set to max
   cmp cur_x, MAX_WIDTH_BORDER
   jle .dont_max_curx
   mov cur_x, MAX_WIDTH_BORDER
.dont_max_curx:
   cmp cur_y, MAX_HEIGHT_BORDER
   jle .dont_max_cury
   mov cur_y, MAX_HEIGHT_BORDER
.dont_max_cury:

; paint snake at cur position
   mov al, '$'
   call draw_at_snake_loc

   mov dl, cur_x
   cmp food_x, dl
   jne .else
   mov dh, cur_y
   cmp food_y, dh
   jne .else
.if:
; if on the same place as food, eat it and put it somewhere else
; add to "score"?
   push word food_x
   push word food_y

   mov dl, food_x
   call rehash_dl
   mov food_x, dl

   mov dl, food_y
   call rehash_dl
   mov food_y, dl

   pop word dx
   cmp dl, food_y
   jne .food_did_change_pos_y
   pop word dx
   cmp dl, byte food_x
   jne .end_food_pos
; food did not change position...
   sub food_x, 2
   add food_y, 1
   jmp .end_food_pos
.food_did_change_pos_y:
   pop word dx
.end_food_pos:

   mov al, '*'
   call draw_at_food_loc
.else:

; height is 24, width is 80

; wait a bit of time and go back to main loop
   call wait_frame

   jmp .main_loop

rehash_dl:
   add al, ah
   mov dl, al
   shl dl, 5
   add dl, 13
   xor dl, 0x47
   ; shr dl, 3
   ; add dl, 5
   and dl, byte 0b11111
   ret

wait_frame:
   mov ah, 0x86
   mov cx, 0x1
   mov dx, 0xffff
   int 0x15
   ret

; draws whatever is at al
draw_at_snake_loc:
   mov dh, cur_y
   mov dl, cur_x
   mov ah, 0x2
   int 0x10
   mov ah, 0xE
;;;mov al, al;;;
   int 0x10
   ret

; draws whatever is at al
draw_at_food_loc:
   mov dh, food_y
   mov dl, food_x
   mov ah, 0x2
   int 0x10
   mov ah, 0xE
;;;mov al, al;;;
   int 0x10
   ret

move_cursor_begin:
   mov dh, 0
   mov dl, 0
   mov ah, 0x2
   int 0x10

   ret

clear_screen:
   call move_cursor_begin

   mov ah, 0xe ; sets up "printing mode"
   mov al, ' ' ; sets up ascii 0 to be printed
   mov dx, 0
.loop:
   int 0x10 ; along ah==0xe, prints the char in al

   inc dx
   cmp dx, 0x780
   jnz .loop

   ret

draw_border:
   call move_cursor_begin

   mov ah, 0xE ; Sets up ah for printing
   mov al, '-'
   mov dx, 0
.loop_top:
   int 0x10 ; along ah==0xe, prints the char in al
   inc dx
   cmp dx, MAX_WIDTH
   jle .loop_top

   mov di, 1
.loop_ys:
; cursor is already in the correct position
   mov al, '|'
   mov ah, 0xE
   int 0x10

   mov dx, di
   shl dx, 8
   mov dl, MAX_WIDTH
   mov ah, 0x2
   int 0x10

   mov al, '|'
   mov ah, 0xE
   int 0x10

   add di, 1
   cmp di, MAX_HEIGHT_BORDER
   jle .loop_ys

   mov ah, 0xE
   mov al, '-'
   mov dx, 0
.loop_bottom:
   int 0x10
   add dx, 1
   cmp dx, MAX_WIDTH
   jle .loop_bottom

   ret
