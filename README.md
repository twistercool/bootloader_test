# Bootload of fun

This mini-project just serves as a way for me to learn about various things relevant to bootloaders (x86 real mode assembly, interfacing with the BIOS, disk sectors, etc...).

It creates a drive image that the BIOS will hand control to, where you can run arbitrary logic from, and writes text to the display.


## Building Process

To build the image, simply run:
```sh
make
```

You can the run the image with an emulator, I personally like `virt-manager`, which is built on top of QEMU/KVM.

It plays a mini version of snake.


## Dependencies

Make sure you have the following installed or available:
- qemu-img
- dd
- nasm
- make
- sh


## Explanation

If you're interested in how this works, here is a summary:

During the boot process of an x86 computer, the first step is to `POST` (`P`ower-`O`n `S`elf-`t`est), which is firmware that performs various hardware tests.

Then, the control is given to the `BIOS` (legacy `BIOS` in our case, as opposed to `UEFI`, which is more modern but slightly more complex), which is firmware that detects hardware and lets you control various parameters like CPU/RAM clock speed and the like.
It then detects bootable drives (it takes the first `512` bytes of a drive aka the boot sector, and if the two last bytes are `0x55 0xAA`, it is considered bootable, assuming `MBR` partitioning), loads that code into `RAM` jumps to it, giving you control of the computer and running the instructions we left there.

Our code is x86 "real-mode" assembly, which is pretty different from modern x86-64 user-space assembly.
There is no virtual memory, so any memory access is direct, and you need to use 2 16-bit registers to store addresses!

You can also directly communicate to the `BIOS` via interupts (instead of communicating with an operating system with syscalls), which means that you directly control the hardware as well.
So if you make changes to this project, make sure you test it on a virtual machine first, you could potentially mess up with your hardware if not careful.

If our code is too long (more than `446` or `510` bytes) and doesn't fit in the boot sector, it needs to be split in two, or even three parts, which hasn't been done here.

From this point, to load a typical OS, a bootloader like `GRUB` would setup determine which partition to boot from, find and load the kernel image into memory and enable protected mode amongst others.
